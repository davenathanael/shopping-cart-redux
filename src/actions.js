import { CART_ADD_ITEM, CART_REMOVE_ITEM, SET_COUPON_CODE } from './constants'

export function addItemToCart(item) {
  return {
    type: CART_ADD_ITEM,
    payload: item,
  }
}

export function removeItemFromCart(itemId) {
  return {
    type: CART_REMOVE_ITEM,
    payload: itemId,
  }
}

export function setCouponCode(code) {
  return {
    type: SET_COUPON_CODE,
    payload: code,
  }
}
