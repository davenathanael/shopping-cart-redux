export const CART_ADD_ITEM = 'cart/CART_ADD_ITEM'
export const CART_REMOVE_ITEM = 'cart/CART_REMOVE_ITEM'

export const SET_COUPON_CODE = 'checkout/SET_COUPON_CODE'

export const SET_SUBTOTAL_COST = 'price/SET_SUBTOTAL_COST'
export const SET_TOTAL_COST = 'price/SET_TOTAL_COST'
export const SET_DISCOUNT = 'price/SET_DISCOUNT'
export const SET_DELIVERY_COST = 'price/SET_DELIVERY_COST'

export const SET_DELIVERY_METHOD = 'delivery/SET_DELIVERY_METHOD'
