export default Array(20)
  .fill(0)
  .map((_, index) => ({
    id: `product-${index + 1}`,
    name: `Product ${index + 1}`,
    price: 20000 + index * 100,
    description: `This is item number ${index + 1}`,
  }))
