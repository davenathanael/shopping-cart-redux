import React, { useState, useEffect } from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'

import ProductItem from './ProductItem'
import dummyProducts from './products'
import { addItemToCart, removeItemFromCart } from './actions'

const Layout = styled.div`
  width: 82.5%;
  margin: 0 auto;
  display: flex;

  .left {
    flex-grow: 1;
    margin-right: 1rem;
  }
`

const CartBox = styled.div`
  border: 1px solid lightgrey;
  padding: 0.5rem;
`

function ProductList({ dispatch, selected }) {
  const [products, setProducts] = useState([])

  useEffect(() => {
    setProducts(dummyProducts)
  }, [])

  return (
    <Layout>
      <div className="left">
        <h1>Product List</h1>
        {products.map((p) => (
          <ProductItem
            key={p.id}
            product={p}
            onClick={() => {
              if (selected.includes(p)) {
                dispatch(removeItemFromCart(p.id))
              } else {
                dispatch(addItemToCart(p))
              }
            }}
            isSelected={selected.includes(p)}
          />
        ))}
      </div>
      <div className="right">
        <h1>Cart</h1>
        <CartBox>
          {selected.length === 0 && 'Shopping cart empty.'}
          {selected.length > 0 && (
            <ul>
              {selected.map((item) => (
                <li key={item.id}>{item.name}</li>
              ))}
            </ul>
          )}
          {selected.length > 0 && <Link to="/checkout">Checkout</Link>}
        </CartBox>
      </div>
    </Layout>
  )
}

const mapStateToProps = (state) => ({
  selected: state.cart,
})

const mapDispatchToProps = (dispatch) => ({ dispatch })

export default connect(mapStateToProps, mapDispatchToProps)(ProductList)
