import React from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import { createStore, combineReducers, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import { Provider } from 'react-redux'
import { composeWithDevTools } from 'redux-devtools-extension'

import ProductList from './ProductList'
import * as reducers from './reducers'
import Checkout from './Checkout'

const appReducer = combineReducers(reducers)

export const store = createStore(
  appReducer,
  composeWithDevTools(applyMiddleware(thunk)),
)

function App() {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <Switch>
          <Route exact path="/" component={ProductList} />
          <Route exact path="/checkout" component={Checkout} />
        </Switch>
      </BrowserRouter>
    </Provider>
  )
}

export default App
