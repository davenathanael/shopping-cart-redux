import { CART_ADD_ITEM, CART_REMOVE_ITEM, SET_COUPON_CODE } from './constants'
import { store } from './App'

export function cart(state = [], action) {
  switch (action.type) {
    case CART_ADD_ITEM:
      return [...state, action.payload]
    case CART_REMOVE_ITEM:
      return state.filter((item) => item.id !== action.payload)
    default:
      return state
  }
}

export function couponCode(state = '', action) {
  switch (action.type) {
    case SET_COUPON_CODE:
      return action.payload
    default:
      return state
  }
}

const initialPriceState = {
  subtotal: 0,
  discount: 0,
  delivery: 0,
  total: 0,
}

export function price(state = initialPriceState, action) {
  switch (action.type) {
    case CART_ADD_ITEM:
    case CART_REMOVE_ITEM:
      return {
        ...state,
        subtotal: store
          .getState()
          .cart.reduce((total, current) => total + current.price, 0),
      }
    default:
      return state
  }
}
