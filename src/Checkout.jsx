import React from 'react'
import { connect } from 'react-redux'
import styled from 'styled-components'

import { setCouponCode } from './actions'

const Layout = styled.div`
  width: 82.5%;
  margin: 0 auto;
`

function Checkout({ cart, couponCode, setCode }) {
  const subtotal = cart.reduce((total, current) => total + current.price, 0)
  const discount = couponCode ? 0.1 * subtotal : 0
  const total = subtotal - discount

  return (
    <Layout>
      <h1>Checkout Page</h1>
      <p>Shopping cart item(s):</p>
      <ul>
        {cart.map((item) => (
          <li key={item.id}>
            {item.name} - {item.price}
          </li>
        ))}
      </ul>
      <p>Subtotal: {subtotal}</p>
      <label htmlFor="coupon-input">Coupon code:</label>
      <input
        id="coupon-input"
        name="coupon-input"
        placeholder="Coupon code here (if any)"
        onChange={(e) => setCode(e.target.value)}
      />
      <p>Total: {total}</p>
    </Layout>
  )
}

const mapStateToProps = (state) => ({
  cart: state.cart,
  couponCode: state.couponCode,
})

const mapDispatchToProps = (dispatch) => ({
  setCode: (code) => dispatch(setCouponCode(code)),
})

export default connect(mapStateToProps, mapDispatchToProps)(Checkout)
