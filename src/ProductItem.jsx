import React from 'react'
import styled from 'styled-components'

const ProductBox = styled.div`
  display: flex;
  padding: 0.5rem;
  margin: 0.5rem 0;
  border: 1px solid lightgrey;
  align-items: center;
  background: ${(props) => (props.isSelected ? 'lightgrey' : 'none')};

  p {
    margin: 0;
  }

  button {
    margin-left: 1rem;
  }
`

function ProductItem({ product, onClick, isSelected }) {
  const { name, price, description } = product

  const handleItemClick = (e) => {
    e.preventDefault()
    onClick()
  }

  return (
    <ProductBox isSelected={isSelected}>
      <div>
        <p>
          {name} - Rp {price}
        </p>
        <small>{description}</small>
      </div>
      <div>
        <button onClick={handleItemClick}>
          {!isSelected ? 'Select' : 'Remove'}
        </button>
      </div>
    </ProductBox>
  )
}

export default ProductItem
